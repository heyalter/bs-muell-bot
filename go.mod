module muellbot

go 1.16

require (
	github.com/go-ini/ini v1.67.0 // indirect
	github.com/ifo/gozulipbot v0.0.1
	golang.org/x/text v0.3.7
)
