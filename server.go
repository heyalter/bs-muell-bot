package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/go-ini/ini"
	"github.com/ifo/gozulipbot"
	"golang.org/x/text/encoding/charmap"
)

type API struct {
	Email string `ini:"email"`
	Key   string `ini:"key"`
	Site  string `ini:"site"`
}

type Config struct {
	API `ini:"api"`
}

func loadConfig(filename string) (*Config, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	cfg, err := ini.Load(file)
	if err != nil {
		return nil, err
	}

	var ret Config
	err = cfg.StrictMapTo(&ret)
	return &ret, err
}

func main() {
	filename := "zuliprc"
	if len(os.Args) > 1 {
		filename = os.Args[1]
	}
	cfg, err := loadConfig(filename)
	if err != nil {
		fmt.Println("Couldn't load config", filename, "because of the error", err)
		return
	}

	var zlbot = gozulipbot.Bot{
		Email:   cfg.API.Email,
		APIKey:  cfg.API.Key,
		APIURL:  cfg.API.Site,
		Backoff: 1 * time.Second,
	}
	zlbot.Init()

	now := time.Now()

	link := "https://alba-bs.de/service/abfuhrtermine/ajax-kalender.html?tx_mfabfallkalender_mfabfallkalender%5Baction%5D=makecsv&tx_mfabfallkalender_mfabfallkalender%5Bcontroller%5D=Abfallkalender&tx_mfabfallkalender_mfabfallkalender%5Bmf-trash-hausnr%5D=1&tx_mfabfallkalender_mfabfallkalender%5Bmf-trash-hausnrzusatz%5D=&tx_mfabfallkalender_mfabfallkalender%5Bmf-trash-strasse%5D=Am%20Wendentor&tx_mfabfallkalender_mfabfallkalender%5Bmf-trash-thisyear%5D=" + strconv.Itoa(now.Year())

	resp, err := http.Get(link)
	if err != nil {
		log.Println("req error", err)
		return
	}
	defer resp.Body.Close()

	rdr := charmap.Windows1252.NewDecoder().Reader(resp.Body)
	crd := csv.NewReader(rdr)
	crd.Comma = ';'

	_, err = crd.Read()
	if err != nil {
		panic(err)
	}

	// TODO expect DATUM;ABFALLART;BEHÄLTERGRÖSSE

	for {
		line, err := crd.Read()
		if err != nil {
			break
		}

		date, err := time.Parse("02.01.2006", line[0])
		if err != nil {
			panic(err)
		}

		diff := date.Sub(now)
		if diff > 0 && diff < 2*24*time.Hour {
			if line[1] == "LVP" {
				line[1] = "Leichtverpackung (Gelbe Tonne)"
			}

			// da wir vom anderen Torhaus den Müllplan nutzen müssen für korrekte Abholzeiten,
			// ignorieren wir hier die Tonnen, die wir nicht nutzen.
			if line[1] == "Biotonne" || line[1] == "Altpapier" {
				continue
			}

			text := fmt.Sprintf("Am %02d wird %s %s abgeholt! Bitte vorher rausstellen.", date.Day(), line[2], line[1])
			if _, err := zlbot.Message(gozulipbot.Message{
				Stream:  "HeyAlter_HQ Braunschweig",
				Topic:   "Müllabholung",
				Content: text,
			}); err != nil {
				log.Println("Couldn't send chat message:", err)
				return
			}
		}
	}
}
